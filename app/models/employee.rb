class Employee < ApplicationRecord
  has_many :activities
  validates :name, presence: true,
                   length: { minimum: 6  }
  validates :password, presence: true,
                   length: { minimum: 10  }
  has_secure_password
  
  
  def Employee.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
end
