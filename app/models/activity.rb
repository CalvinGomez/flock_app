class Activity < ApplicationRecord
  before_save :update_check_in
  belongs_to :employee
  
  def update_check_in
    activity = Activity.where(employee_id: self.employee_id)
    if activity.length > 0
      self.check_in = activity.last.check_in == true ? false:true
    else
      self.check_in = true
    end
  end
end
