class StaticPagesController < ApplicationController
  def home
    if logged_in?
      redirect_to current_employee
    else
      render 'static_pages/home'
    end
  end
end
