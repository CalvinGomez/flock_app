class EmployeesController < ApplicationController
  def new
  end
  
  def index
    if logged_in?
      if current_employee.admin?
        @employees = Employee.all
      else
        redirect_to current_employee
      end
    else
      redirect_to login_path
    end
  end
  
  def show
    if logged_in?
      if params[:id] == session[:employee_id].to_s
        @employee = Employee.find_by(id: session[:employee_id])
        @next_activity = get_next_activity @employee
        @activities = get_activities @employee
      else
        if current_employee.admin?
          @employee = Employee.find_by(id: params[:id])
          if @employee.nil?
            redirect_to current_employee
          else
            @next_activity = get_next_activity @employee
            @activities = get_activities @employee
          end
        else
          redirect_to current_employee
        end
      end
    else
      redirect_to login_path
    end
  end
end
