class ActivityController < ApplicationController
  def new
    if logged_in?
      new_activity = Activity.new(employee_id: session[:employee_id], check_in: true)
      new_activity.save
      redirect_to current_employee
    else
      redirect_to login_path
    end
  end
end
