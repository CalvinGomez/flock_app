class SessionsController < ApplicationController
  def new
    if logged_in?
      redirect_to current_employee
    else
      render 'new'
    end
  end
  
  def create
    employee = Employee.find_by(id: params[:session][:id])
    if employee && employee.authenticate(params[:session][:password])
      flash[:success] = 'Successfully Logged In!'
      log_in(employee)
      redirect_to employee
    else
      flash.now[:danger] = 'Incorrect Login Credentials'
      render 'new'
    end
  end
  
  def destroy
    log_out
    redirect_to login_path
  end
end
