module ActivityHelper
  def get_next_activity(employee)
    next_activity = Activity.where(employee_id: employee.id).last.check_in == false ? 'Check In' : 'Check Out'
    return next_activity
  end
  
  def get_activities(employee)
    if employee.admin?
      emp_activities = Employee.includes(:activities)
      activities = restructure_activities(emp_activities)
      activities = activities.sort_by { |activity| activity['updated_at'] }
    else
      emp_activities = Employee.includes(:activities).where(id: employee.id)
      activities = restructure_activities(emp_activities)
    end
    return activities
  end
  
  def restructure_activities(emp_activities)
    activities = Array.new()
    for emp in emp_activities
      for activity in emp.activities
        activities.push({
          "id" => emp.id,
          "name" => emp.name,
          "check_in" => activity.check_in == true ? "Checked In" : "Checked Out",
          "updated_at" => activity.updated_at
        })
      end
    end
    return activities
  end
end
