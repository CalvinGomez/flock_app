employees = Employee.create([
                { name: 'Chirag', admin: true, password: '123456789123', password_confirmation: '123456789123' }, 
                { name: 'Akshay', admin: false, password: 'abcdefghijkl', password_confirmation: 'abcdefghijkl' }, 
                { name: 'Calvin', admin: false, password: '987654321987', password_confirmation: '987654321987' }
            ])
activities = Activity.create([
                { employee: employees.first, check_in: true }, 
                { employee: employees.first, check_in: false },
                { employee: employees.second, check_in: true },
                { employee: employees.second, check_in: false },
                { employee: employees.second, check_in: true },
                { employee: employees.second, check_in: false },
                { employee: employees.first, check_in: true },
                { employee: employees.third, check_in: true },
                { employee: employees.first, check_in: false },
                { employee: employees.third, check_in: false },
                { employee: employees.second, check_in: true },
            ])