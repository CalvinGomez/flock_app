class AddCheckInToActivities < ActiveRecord::Migration[5.1]
  def change
    add_column :activities, :check_in, :boolean
  end
end
