class CreateActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :activities do |t|
      t.belongs_to :employee, foreign_key: true
      t.string :activity

      t.timestamps
    end
  end
end
