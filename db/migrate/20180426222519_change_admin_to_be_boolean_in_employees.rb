class ChangeAdminToBeBooleanInEmployees < ActiveRecord::Migration[5.1]
  def change
    change_column :employees, :admin, 'boolean USING CAST(admin AS boolean)'
  end
end
