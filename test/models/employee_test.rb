require 'test_helper'

class EmployeeTest < ActiveSupport::TestCase
  
  def setup
    @employee = Employee.new(name: "Calvin Gomez", admin: true, password: "!6f@9QmEy$$cYWVtW$Vr", password_confirmation: "!6f@9QmEy$$cYWVtW$Vr")
  end
  
  test "employee should not have blank name" do
    @employee.name = " " * 4
    assert_not @employee.valid?
  end
  
  test "employee name should be atleast 6 characters" do
    @employee.name = "a" * 5
    assert_not @employee.valid?
  end
  
  test "employee password should be atleast 10 characters" do
    @employee.password = "abcde"
    @employee.password_confirmation = "abcde"
    assert_not @employee.valid?
  end
  
  test "employee password should not be blank" do
    @employee.password = " " * 10
    @employee.password_confirmation = " " * 10
    assert_not @employee.valid?
  end
end
