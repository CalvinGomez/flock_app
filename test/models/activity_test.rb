require 'test_helper'

class ActivityTest < ActiveSupport::TestCase
  def setup
    @employee = employees(:calvin)
    @employee.save
    @activity = Activity.new(employee_id:  @employee.id, check_in: true)
  end
  
  test "activity should belong to an existing employee" do
    @activity.employee_id = 1
    assert_not @activity.valid?
  end
end
