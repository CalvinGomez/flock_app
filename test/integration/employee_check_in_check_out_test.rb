require 'test_helper'

class EmployeeCheckInCheckOutTest < ActionDispatch::IntegrationTest
  def setup
    @employee = employees(:calvin)
  end
  
  test "button text should be opposite of last activity" do
    post login_path, params: { session: { id: @employee.id, password: 'password'  }  }
    assert_redirected_to @employee
    follow_redirect!
    assert_select "a", get_next_activity(@employee)
  end
end
