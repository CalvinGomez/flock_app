require 'test_helper'

class EmployeeLoginTest < ActionDispatch::IntegrationTest
  def setup
    @employee = employees(:calvin)
    @employee2 = employees(:xavier)
  end
  
  test "should render application home page when logged out and attempting to go to root" do
    get root_path
    assert_select "a[href=?]", login_path, count: 2
    assert_select "h1", "Welcome to the Flock App", count: 1
    assert_template 'static_pages/home'
  end
  
  test "should login with correct credentials" do
    get login_path
    post login_path, params: { session: { id: @employee.id, password: 'password'  }  }
    assert_redirected_to @employee
    follow_redirect!
    assert_template 'employees/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", employee_path(@employee)
    assert_select "table", count: 1
  end
  
  test "should reload login page with flash message if incorrect credentials" do
    get login_path
    post login_path, params: { session: { id: @employee.id, password: 'password123'  }  }
    assert_template 'sessions/new'
    assert_select "div", "Incorrect Login Credentials", count: 1
    post login_path, params: { session: { id: 26, password: 'password123'  }  }
    assert_template 'sessions/new'
    assert_select "div", "Incorrect Login Credentials", count: 1
  end
  
  test "going to /login should redirect to employee home page if already logged in" do
    post login_path, params: { session: { id: @employee.id, password: 'password'  }  }
    follow_redirect!
    get login_path
    follow_redirect!
    assert_template 'employees/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", employee_path(@employee)
    assert_select "table", count: 1
  end
  
  test "should redirect to login page after logout" do
    post login_path, params: { session: { id: @employee.id, password: 'password'  }  }
    follow_redirect!
    delete logout_path
    assert_redirected_to login_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", employee_path(@employee), count: 0
  end
  
  test "should redirect to login page if logged out and attempting to access employee page" do
    get employee_path @employee
    assert_redirected_to login_path
    follow_redirect!
    assert_template 'sessions/new'
  end
  
  test "should redirect to employee home page if logged in and attempting to access application home page" do
    post login_path, params: { session: { id: @employee.id, password: 'password'  }  }
    follow_redirect!
    get root_path
    assert_redirected_to employee_path(@employee)
  end
end
