require 'test_helper'

class AdminTestTest < ActionDispatch::IntegrationTest
  def setup
    @employee = employees(:calvin)
    @employee2 = employees(:xavier)
  end
  
  test "should redirect to employee 2 if non-admin employee 2 attempts to access employee" do
    post login_path, params: { session: { id: @employee2.id, password: 'password1'  }  }
    follow_redirect!
    get employee_path @employee
    assert_redirected_to employee_path(@employee2)
  end
  
  test "should redirect to employees index page if admin attempts to access" do
    post login_path, params: { session: { id: @employee.id, password: 'password'  }  }
    follow_redirect!
    get employees_path
    assert_response :success
    assert_template 'employees/index'
  end
  
  test "should redirect to employee home if non-admin attempts to access employees index page" do
    post login_path, params: { session: { id: @employee2.id, password: 'password1'  }  }
    follow_redirect!
    get employees_path
    assert_redirected_to @employee2
  end
  
  test "should redirect to login page if non-logged-in employee attempts to access employees index page" do
    get employees_path
    assert_redirected_to login_path
  end
  
  test "admin check in/check out button should only appear on admin home page" do
    post login_path, params: { session: { id: @employee.id, password: 'password'  }  }
    follow_redirect!
    get employees_path
    assert_select "button", count: 0
    get employee_path @employee2
    assert_select "button", count: 0
  end
  
  test "admin back to home link should only appear on non-home pages for admin" do
    post login_path, params: { session: { id: @employee.id, password: 'password'  }  }
    follow_redirect!
    get employees_path
    assert_select "a", "Back to Home", count: 1
    get employee_path @employee2
    assert_select "a", "Back to Home", count: 1
  end
  
  test "admin menu should include links for activity and employees" do
    post login_path, params: { session: { id: @employee.id, password: 'password'  }  }
    follow_redirect!
    assert_select "a", "View Employees", count: 1
    assert_select "a", "View Activity", count: 1
  end
end
